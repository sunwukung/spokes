import React from 'react'
import { Router } from '@reach/router'

const Route = ({ config }, i) => {
  return <config.component key={i} path={config.route} />
}

export const Routes = ({ routes }) => {
  return (
    <Router>
      {routes.map(Route)}
    </Router>
  )
}