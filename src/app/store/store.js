import { createStore, applyMiddleware, combineReducers } from 'redux'
import thunk from 'redux-thunk'
import { composeWithDevTools } from "redux-devtools-extension";
import { reducer } from './modules'
import { xhr } from './middleware'


const buildPluginReducer = (plugins) => combineReducers(
  plugins.reduce((acc, plugin) => {
    acc[plugin.name] = plugin.reducer
    return acc
  }, {})
)

const buildRootReducer = (appReducer, plugins) => {
  return combineReducers({
    app: appReducer,
    plugins: buildPluginReducer(plugins)
  })
}
export const configureStore = (plugins, initialState = {}) => {
  const rootReducer = buildRootReducer(reducer, plugins)
  console.log(plugins)
  const middlewares = [xhr, thunk]
  const middlewareEnhancer = applyMiddleware(...middlewares)

  const store = createStore(
    rootReducer,
    initialState,
    composeWithDevTools(middlewareEnhancer)
  )

  if (module.hot) {
    module.hot.accept('./modules', () => {
      const nextRootReducer = require('./modules');
      store.replaceReducer(nextRootReducer);
    });
  }

  return store;
}