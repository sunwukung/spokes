import { reducer as ui } from './ui'
import { combineReducers } from 'redux'

export const reducer = combineReducers({
  ui
})