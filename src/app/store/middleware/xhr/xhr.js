import { XHR_FETCH, XHR_SUCCESS, XHR_FAIL } from './constants'
import axios from 'axios'

export const xhr = (store) => (next) => ({ type, payload, meta }) => {
  if (type === XHR_FETCH) {
    return next((dispatch) => {
      const method = meta.method
      return axios[method](meta.url, payload)
        .then((res) => {
          dispatch({
            type: XHR_SUCCESS,
            payload: res,
            meta
          })
        })
        .catch((e) => {
          dispatch({
            type: XHR_FAIL,
            payload: {
              error: e
            },
            meta
          })
        })
    })
  }
  next({ type, payload, meta })
}
