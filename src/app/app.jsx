import React from 'react'
import { Header } from 'ui/patterns/header'
import { Routes } from './components'
import base from './base.scss'
import { ApplicationApi } from '../lib'

export const App = ({ plugins }) => {
  return (
    <div>
      <Header
        title="plugins POC"
        links={plugins.map(plugin => ({ title: plugin.name, path: plugin.config.route }))} />
      <Routes routes={plugins} ctx={ApplicationApi} />
    </div>
  )
}