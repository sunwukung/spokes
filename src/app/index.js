import { hot } from 'react-hot-loader/root';
import React from 'react'
import ReactDOM from 'react-dom'
import { Provider } from 'react-redux'
import { configureStore } from './store'
import { App } from './app'

const RootApp = hot(({ plugins }) => {
  return (
    <Provider store={configureStore(plugins, {})}>
      <App plugins={plugins} />
    </Provider>
  )
})

export const start = (plugins) => {
  ReactDOM.render(
    <RootApp plugins={plugins} />,
    document.getElementById("app")
  );
}