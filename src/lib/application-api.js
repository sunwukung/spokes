import { connect as con } from 'react-redux'
export class ApplicationApi {
  constructor({ config, features, plugin }) {
    this.config = config
    this.features = features
    this.plugin = plugin
  }
  getConfig = (key) => key ? this.config[key] : this.config
  getFeatures = () => this.features
  featureEnabled = (key) => this.features[key]
  connect = (mapState, mapDispatch) => {
    const local = (state, ownProps) => {
      return mapState(state[this.plugin], ownProps)
    }
    return con(local, mapDispatch)
  }
}