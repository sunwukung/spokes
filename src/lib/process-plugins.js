export const processPlugins = (api) => (manifest) => {
  return manifest.map((mod) => {
    let plugin = window[mod].default(api)
    delete window[mod]
    return plugin
  })
}