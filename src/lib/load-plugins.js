import { all } from 'simple-load-script'

const serverAddress = 'http://localhost:9000'

export const loadPlugins = ({ manifest }) => {
  const plugins = manifest.map((plugin) => `${serverAddress}/${plugin}.js`)
  return all.apply(null, plugins)
    .then(function (scriptRef) {
      console.log('success', scriptRef);
      return manifest
    })
}
