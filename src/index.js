import axios from 'axios'
import { start } from './app'
import { ApplicationApi, loadPlugins, processPlugins } from 'lib'

const Api = new ApplicationApi({
  config: {
    url: 'http://something/important.com',
    timeout: 3000
  },
  features: {}
})

axios.get('http://localhost:3000/manifest')
  .then(({ data }) => loadPlugins(data))
  .then(processPlugins(Api))
  .then(start)
  .catch((e) => console.log(e))