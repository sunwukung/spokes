import React from 'react'
import PropTypes from 'prop-types'
import cx from 'classnames'
import style from './title.scss'

export const Title = ({ text, size }) => {
  switch (size) {
    case 'max':
      return <h1 className={cx(style.title, style.max)}>{text}</h1>
    case 'med':
      return <h2 className={cx(style.title, style.med)}>{text}</h2>
    case 'min':
      return <h3 className={cx(style.title, style.min)}>{text}</h3>
    default:
      return <h1>{text}</h1>
  }
}

Title.propTypes = {
  text: PropTypes.string,
  size: PropTypes.string,
}