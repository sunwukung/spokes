import React from 'react'
import PropTypes from 'prop-types'
import { Link } from '@reach/router'
import style from './navigation.scss'

export const Navigation = ({ links }) => {
  return (
    <nav className={style.navigation}>
      {links.map(link => <Link key={link.title} to={link.path}>{link.title}</Link>)}
    </nav>
  )
}

Navigation.propTypes = {
  links: PropTypes.array
}