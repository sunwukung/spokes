import React from 'react'
import PropTypes from 'prop-types'
import { Title } from '../../typography/title/title'
import { Navigation } from '../navigation'
import style from './header.scss'

export const Header = ({ title, links }) => {
  return (
    <header className={style.header}>
      <Title text={title} size="med" />
      <Navigation links={links} />
    </header>
  )
}

Header.propTypes = {
  title: PropTypes.string,
  links: PropTypes.array
}


