const path = require('path');

const plugins = ['./src/foo', './src/bar'].map(plugin => path.resolve(__dirname, plugin))

const pluginRules = plugins.map((plugin) => {
  const name = plugin.slice(plugin.lastIndexOf('/') + 1)
  return {
    test: plugin,
    exclude: /node_modules/,
    use: [
      {
        loader: 'expose-loader',
        options: name
      },
      {
        loader: 'babel-loader',
        options: {
          presets: ['@babel/preset-env', '@babel/preset-react']
        }
      },
    ],
  }
})

const config = {
  entry: {
    foo: './src/foo/index.js',
    bar: './src/bar/index.js'
  },
  output: {
    path: path.resolve(__dirname, 'dist'),
    filename: '[name].js'
  },
  module: {
    rules: pluginRules
  },
  externals: ['react', 'ramda', 'prop-types'],
  devServer: {
    contentBase: path.join(__dirname, './dist'),
    compress: true,
    port: 9000,
    proxy: {
      '/plugins': {
        target: {
          host: "0.0.0.0",
          protocol: 'http:',
          port: 9000
        },
        pathRewrite: {
          '^/plugins': ''
        }
      }
    }
  },
  resolve: {
    extensions: [
      '.js',
    ]
  }
}

module.exports = config;