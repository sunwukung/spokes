import React from 'react'

export const Bar = ({ api }) => {
  const timeout = api.getConfig('timeout')
  return (
    <div>
      <h1>Bar Plugin</h1>
      <p>value: {timeout}</p>
    </div>
  )
}
