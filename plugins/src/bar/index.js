import React from 'react'
import { Bar } from './bar'
import { reducer } from './reducer'

export default (api) => ({
  name: 'bar',
  reducer,
  config: {
    route: '/bar',
    component: () => <Bar api={api} />
  }
})