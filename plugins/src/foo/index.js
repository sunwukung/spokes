import React from 'react'
import { Foo } from './foo'
import { reducer } from './reducer'

export default (api) => ({
  name: 'foo',
  reducer,
  config: {
    route: '/foo',
    component: () => <Foo api={api} />
  }
})