# Spokes

POC to demonstrate plugin architecture in a React/Redux application

## Development

```
npm run prep // installs all dependencies including sub-apps
npm run start:stub // serves plugin manifest
npm run start:plugins // serves plugin content with WDS
npm run start // serves host app with WDS & HMR
```

## Principles

- Fetch sub-application content over the wire
- Utilise Inversion of Control / DI - feed dependencies to plugins, avoid imports.
- Code to an Interface, not an implementation
- Avoid direct references to the host application internals
- Prevent global namespace pollution by deleting window references once loaded
- Export vendor packages to global namespace, treat as externals in plugins
- Use Context/DI to provide Application functionality (routing, redux etc)
- Insulate sub-applications from each others state, don't let `connect` open a backdoor
- attach to a protected slice in the store
- Avoid parent app redux store, prefer Component state
- Implement types to assert roles, Components are too generic
- use index files to provide local dependencies
