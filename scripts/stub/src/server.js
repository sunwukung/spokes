const path = require('path')
const express = require('express')
const cors = require('cors')

const port = 3000
let app = express()
app.use(cors());
app = require('./resources')(app)

module.exports = () => {
  app.listen(port, () => console.log(`Example app listening on port ${port}!`))
}