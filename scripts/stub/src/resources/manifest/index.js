module.exports = (app) => {
  app.get('/manifest', (req, res) => {
    res.json({
      manifest: [
        'foo',
        'bar'
      ]
    })
  })
  return app
}