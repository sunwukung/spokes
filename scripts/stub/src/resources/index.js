const R = require('ramda')
const manifest = require('./manifest')

module.exports = R.compose(
  manifest
)