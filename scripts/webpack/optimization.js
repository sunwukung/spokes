module.exports = {
  chunkIds: "named",
  splitChunks: {
    cacheGroups: {
      styles: {
        test: /\.(sa|sc|c)ss$/,
        chunks: 'all',
        enforce: true,
      },
      vendor: {
        test: /node_modules/,
        chunks: "initial",
        name: "vendor",
        priority: 10,
        enforce: true
      }
    },
  },
}