module.exports = {
  clean: require('./clean'),
  css: require('./css'),
  html: require('./html')
}