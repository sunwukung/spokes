const path = require('path');
const HtmlWebpackPlugin = require('html-webpack-plugin')

module.exports = new HtmlWebpackPlugin({
  title: 'SWK:boilerplate',
  meta: {
    viewport: 'width=device-width, initial-scale=1, shrink-to-fit=no',
    charset: 'utf-8'
  },
  template: path.resolve(__dirname, './index.html'),
  hash: true,
})