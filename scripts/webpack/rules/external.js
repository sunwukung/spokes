module.exports = ['react', 'ramda', 'prop-types'].map(name => {
  return {
    test: require.resolve(name),
    use: [{
      loader: 'expose-loader',
      options: name
    }]
  }
})