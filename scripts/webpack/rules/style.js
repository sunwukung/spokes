const path = require('path')
const MiniCssExtractPlugin = require('mini-css-extract-plugin');
const isDevMode = process.env.NODE_ENV !== 'production';

const styleLoader = isDevMode
  ? { loader: 'style-loader' }
  : {
    loader: MiniCssExtractPlugin.loader,
    options: {
      filename: isDevMode ? '[name].css' : '[name].[hash].css',
      chunkFilename: isDevMode ? '[id].css' : '[id].[hash].css',
      hmr: process.env.NODE_ENV === 'development',
      reloadAll: true
    }
  }

module.exports = {
  test: /\.(sa|sc|c)ss$/,
  use: [
    styleLoader,
    {
      loader: 'css-loader',
      options: {
        importLoaders: 1,
        modules: true,
        localsConvention: 'camelCase',
      }
    },
    {
      loader: 'postcss-loader',
      options: {
        config: {
          path: path.resolve(__dirname, './../../../postcss.config.js')
        }
      },
    }
  ]
}