module.exports = {
  external: require('./external'),
  file: require('./file'),
  js: require('./js'),
  style: require('./style')
}