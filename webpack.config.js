const path = require('path');
const { css, clean, html } = require('./scripts/webpack/plugins');
const { external, file, js, style } = require('./scripts/webpack/rules');
const optimization = require('./scripts/webpack/optimization');

const config = {
  cache: false,
  entry: ['./src/index.js'],
  output: {
    path: path.resolve(__dirname, 'dist'),
    filename: '[name].[hash].js',
    publicPath: '/'
  },
  module: {
    rules: [
      ...external,
      file,
      js,
      style,
    ]
  },
  plugins: [
    clean,
    css,
    html,
  ],
  resolve: {
    alias: {
      ui: path.resolve(__dirname, './src/ui'),
      lib: path.resolve(__dirname, './src/lib'),
      'react-dom': '@hot-loader/react-dom'
    },
    extensions: [
      '.js',
      '.jsx'
    ]
  },
  optimization,
  devtool: "source-map",
  devServer: {
    historyApiFallback: true,
    hot: true
  }
}

module.exports = config;